const { assert } = require("chai");
const { getCircleArea } = require("../index.js");

describe("Test get circle area", () => {
	it("Test area of circle with radius 15 is 706.86", () => {
		let area = getCircleArea(15);
		assert.equal(area, 706.86); 
	})
	it("Test area of circle with negative radius is undefined", () => {
		let area = getCircleArea(-1);
		assert.equal(area, undefined);
	})
	it("Test area of cirle with 0 radius is undefined", () => {
		let area = getCircleArea(0);
		assert.isUndefined(area);
	})
	it("Test area of circle with non-numeric value - returns undefined", () => {
		let area = getCircleArea("1");
		assert.isUndefined(area);
	})
})