const express = require("express");
const app = express();
const port = 5001;
let userRoutes = require("./app/routes");

app.use(express.json());

app.use("/users", userRoutes);

app.listen(port, () => console.log(`Running on port: ${port}`));